import { applyDecorators } from '@nestjs/common'
import {
  ApiOperation,
  ApiExtraModels,
  ApiOkResponse,
  getSchemaPath,
} from '@nestjs/swagger'
import { Messages } from '../../common/constants/response-messages'
import { SuccessResponseDto } from '../dto/success-response.dto'

export const ApiDocSuccessList = (Descripcion = '', ModelDTO: any) => {
  return applyDecorators(
    ApiOperation({ summary: Descripcion }),
    ApiExtraModels(ModelDTO),
    ApiOkResponse({
      schema: {
        properties: {
          finalizado: {
            type: 'boolean',
            example: true,
          },
          mensaje: {
            type: 'string',
            example: Messages.SUCCESS_LIST,
          },
          datos: {
            type: 'object',
            properties: {
              total: {
                type: 'number',
                example: 1,
              },
              filas: {
                type: 'array',
                items: { $ref: getSchemaPath(ModelDTO) },
              },
            },
          },
        },
      },
    })
  )
}

export const ApiDocSuccessCreate = (Descripcion = '', ModelDTO: any) => {
  return applyDecorators(
    ApiOperation({ summary: Descripcion }),
    ApiExtraModels(SuccessResponseDto),
    ApiExtraModels(ModelDTO),
    ApiOkResponse({
      schema: {
        properties: {
          finalizado: {
            type: 'boolean',
            example: true,
          },
          mensaje: {
            type: 'string',
            example: Messages.SUCCESS_CREATE,
          },
          datos: {
            $ref: getSchemaPath(ModelDTO),
          },
        },
      },
    })
  )
}

export const ApiDocSuccessUpdate = (Descripcion = '') => {
  return applyDecorators(
    ApiOperation({ summary: Descripcion }),
    ApiExtraModels(SuccessResponseDto),
    ApiOkResponse({
      schema: {
        properties: {
          finalizado: {
            type: 'boolean',
            example: true,
          },
          mensaje: {
            type: 'string',
            example: Messages.SUCCESS_UPDATE,
          },
        },
      },
    })
  )
}

export const ApiDocSuccessDelete = (Descripcion = '') => {
  return applyDecorators(
    ApiOperation({ summary: Descripcion }),
    ApiExtraModels(SuccessResponseDto),
    ApiOkResponse({
      schema: {
        properties: {
          finalizado: {
            type: 'boolean',
            example: true,
          },
          mensaje: {
            type: 'string',
            example: Messages.SUCCESS_DELETE,
          },
        },
      },
    })
  )
}

export const ApiDocSuccess = (Descripcion = '', ModelDTO?: any) => {
  if (ModelDTO) {
    return applyDecorators(
      ApiOperation({ summary: Descripcion }),
      ApiExtraModels(SuccessResponseDto),
      ApiExtraModels(ModelDTO),
      ApiOkResponse({
        schema: {
          properties: {
            finalizado: {
              type: 'boolean',
              example: true,
            },
            mensaje: {
              type: 'string',
              example: Messages.SUCCESS_DEFAULT,
            },
            datos: {
              $ref: getSchemaPath(ModelDTO),
            },
          },
        },
      })
    )
  }
  return applyDecorators(
    ApiOperation({ summary: Descripcion }),
    ApiExtraModels(SuccessResponseDto),
    ApiOkResponse({
      schema: {
        properties: {
          finalizado: {
            type: 'boolean',
            example: true,
          },
          mensaje: {
            type: 'string',
            example: Messages.SUCCESS_DEFAULT,
          },
        },
      },
    })
  )
}
