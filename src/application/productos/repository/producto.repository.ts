import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { ActualizarProductoDto, CrearProductoDto, FiltroProducto } from '../dto'
import { Producto } from '../entity'
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'

@Injectable()
export class ProductoRepository {
  constructor(private dataSource: DataSource) {}

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    const { codigo, nombre, categoria, precio } = productoDto

    const producto = new Producto()
    producto.codigo = codigo
    producto.nombre = nombre
    producto.categoria = categoria
    producto.precio = precio
    producto.usuarioCreacion = usuarioAuditoria

    return await this.dataSource.getRepository(Producto).save(producto)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto
    const query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id',
        'producto.codigo',
        'producto.nombre',
        'producto.categoria',
        'producto.precio',
        'producto.estado',
      ])
      .take(limite)
      .skip(saltar)

    return await query.getManyAndCount()
  }

  async buscarCodigo(codigo: string) {
    return this.dataSource
      .getRepository(Producto)
      .findOne({ where: { codigo: codigo } })
  }

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: id })
      .getOne()
  }

  async actualizar(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    const datosActualizar: QueryDeepPartialEntity<Producto> = new Producto({
      ...productoDto,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Producto)
      .update(id, datosActualizar)
  }

  async eliminar(id: string) {
    return await this.dataSource.getRepository(Producto).delete(id)
  }

  async listarPorFiltroRep(filtroProductos: FiltroProducto) {
    const { limite, saltar, nombre, categoria } = filtroProductos
    const query = this.dataSource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .select([
        'producto.id', //
        'producto.codigo',
        'producto.nombre',
        'producto.categoria',
        'producto.precio',
        'producto.estado',
      ])
      .take(limite)
      .skip(saltar)

    if (nombre) {
      query.where('producto.nombre = :nombre', { nombre })
    } else if (categoria) {
      query.where('producto.categoria = :categoria', { categoria })
    }
    return await query.getManyAndCount()
  }
}
