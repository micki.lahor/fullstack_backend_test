import { BaseService } from '../../../common/base/base-service'
import {
  ConflictException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { ProductoRepository } from '../repository'
import { CrearProductoDto, FiltroProducto } from '../dto'
import { PaginacionQueryDto } from '../../../common/dto/paginacion-query.dto'
import { ActualizarProductoDto } from '../dto'
import { ProductoEstado } from '../constant'

@Injectable()
export class ProductosService extends BaseService {
  constructor(
    @Inject(ProductoRepository)
    private productoRepositorio: ProductoRepository
  ) {
    super()
  }

  async crear(productoDto: CrearProductoDto, usuarioAuditoria: string) {
    const productoRepetido = await this.productoRepositorio.buscarCodigo(
      productoDto.codigo
    )

    if (productoRepetido) {
      throw new ConflictException(
        `Ya existe un producto con el codigo "${productoDto.codigo}"`
      )
    }

    return await this.productoRepositorio.crear(productoDto, usuarioAuditoria)
  }

  async listar(paginacionQueryDto: PaginacionQueryDto) {
    return await this.productoRepositorio.listar(paginacionQueryDto)
  }

  async actualizarDatos(
    id: string,
    productoDto: ActualizarProductoDto,
    usuarioAuditoria: string
  ) {
    const producto = await this.productoRepositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    await this.productoRepositorio.actualizar(id, productoDto, usuarioAuditoria)
    return { id }
  }

  async buscarPorId(id: string) {
    const producto = await this.productoRepositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    return await this.productoRepositorio.buscarPorId(id)
  }

  async eliminar(id: string) {
    const producto = await this.productoRepositorio.buscarPorId(id)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    const eliminado = await this.productoRepositorio.eliminar(id)
    return { id, eliminado }
  }

  async listarPorFiltro(filtro: FiltroProducto) {
    const result = await this.productoRepositorio.listarPorFiltroRep(filtro)
    return result
  }

  async inactivar(idProducto: string, usuarioAuditoria: string) {
    const parametro = await this.productoRepositorio.buscarPorId(idProducto)
    if (!parametro) {
      throw new NotFoundException('No existe el producto')
    }
    const productoDto = new ActualizarProductoDto()
    productoDto.estado = ProductoEstado.INACTIVO
    await this.productoRepositorio.actualizar(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return { id: idProducto, estado: productoDto.estado }
  }

  async activar(idProducto: string, usuarioAuditoria: string) {
    const producto = await this.productoRepositorio.buscarPorId(idProducto)
    if (!producto) {
      throw new NotFoundException('No existe el producto')
    }
    const productoDto = new ActualizarProductoDto()
    productoDto.estado = ProductoEstado.ACTIVO
    await this.productoRepositorio.actualizar(
      idProducto,
      productoDto,
      usuarioAuditoria
    )
    return {
      id: idProducto,
      estado: productoDto.estado,
    }
  }
}
