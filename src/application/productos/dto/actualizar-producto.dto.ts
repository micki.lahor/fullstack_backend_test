import { ApiProperty } from '@nestjs/swagger'
import { IsOptional } from '../../../common/validation'

export class ActualizarProductoDto {
  @ApiProperty({ example: 'A-00002' })
  @IsOptional()
  codigo: string

  @ApiProperty({ example: 'Nombre del producto' })
  @IsOptional()
  nombre: string

  @ApiProperty({ example: 'Categoria del producto' })
  @IsOptional()
  categoria: string

  @ApiProperty({ example: '59.99' })
  @IsOptional()
  precio: number

  @ApiProperty({ example: 'INACTIVO' })
  @IsOptional()
  estado?: string
}
