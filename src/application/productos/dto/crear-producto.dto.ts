import { IsNotEmpty, IsNumber, IsString } from '../../../common/validation'
import { ApiProperty } from '@nestjs/swagger'

export class CrearProductoDto {
  @ApiProperty({ required: true, example: 'A-00001' })
  @IsNotEmpty()
  @IsString()
  codigo: string

  @ApiProperty({ required: true, example: 'Nombre del producto' })
  @IsNotEmpty()
  @IsString()
  nombre: string

  @ApiProperty({ required: true, example: 'Categoria del producto' })
  @IsNotEmpty()
  @IsString()
  categoria: string

  @ApiProperty({ required: true, example: 99.99 })
  @IsNotEmpty()
  // @IsNumber()
  precio: number
}

export class RespuestaCrearProductoDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  id: string
}
