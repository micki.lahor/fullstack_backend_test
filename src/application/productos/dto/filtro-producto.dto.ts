import { ApiProperty } from '@nestjs/swagger'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'
import { IsOptional } from 'src/common/validation'

export class FiltroProducto extends PaginacionQueryDto {
  @ApiProperty({ example: 'Nombre del producto' })
  @IsOptional()
  nombre: string

  @ApiProperty({ example: 'Categoria del producto' })
  @IsOptional()
  categoria: string
}
