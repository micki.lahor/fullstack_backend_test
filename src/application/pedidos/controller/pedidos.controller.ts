import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common'
import { BaseController } from '../../../common/base'
import { JwtAuthGuard } from '../../../core/authentication/guards/jwt-auth.guard'
import { CasbinGuard } from '../../../core/authorization/guards/casbin.guard'
import { PedidoService } from '../service/pedidos.service'
import { CrearPedidoDto, PedidoFiltro } from '../dto'
import { Request } from 'express'
import { ParamIdDto } from 'src/common/dto/params-id.dto'
import { ActualizarPedidoDto } from '../dto/actualizar-pedido.dto'

@Controller('pedidos')
@UseGuards(JwtAuthGuard, CasbinGuard)
export class PedidosController extends BaseController {
  constructor(private pedidosService: PedidoService) {
    super()
  }

  @Post()
  async crear(@Req() req: Request, @Body() datos: CrearPedidoDto) {
    const usuarioAuditoria = this.getUser(req)
    const result: any = await this.pedidosService.crear(datos, usuarioAuditoria)
    return this.successCreate(result)
  }

  @Get()
  async listar(@Query() consulta: PedidoFiltro) {
    const resultado = await this.pedidosService.listar(consulta)
    return this.successListRows(resultado)
  }

  @Patch(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() pedidoDto: ActualizarPedidoDto
  ) {
    const { id: idPedido } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.pedidosService.actualizarDatos(
      idPedido,
      pedidoDto,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }

  @Patch('/:id/activacion')
  async activar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idPedido } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.pedidosService.activar(idPedido, usuarioAuditoria)
    return this.successUpdate(result)
  }

  @Patch('/:id/inactivacion')
  async inactivar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idPedido } = params
    const usuarioAuditoria = this.getUser(req)
    const result = await this.pedidosService.inactivar(
      idPedido,
      usuarioAuditoria
    )
    return this.successUpdate(result)
  }
}
