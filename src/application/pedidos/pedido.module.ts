import { Module } from '@nestjs/common'
import { PedidosController } from './controller'
import { PedidoService } from './service'
import { ProductosModule } from '../productos/productos.module'
import { PedidoRepository } from './repository'

@Module({
  controllers: [PedidosController],
  providers: [PedidoService, PedidoRepository],
  imports: [ProductosModule],
})
export class PedidosModule {}
