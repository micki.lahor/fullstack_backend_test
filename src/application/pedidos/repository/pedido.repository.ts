import { DataSource } from 'typeorm'
import { Injectable } from '@nestjs/common'
import { Pedido } from '../entity'
import { CrearPedidoDto, PedidoFiltro } from '../dto'
import dayjs from 'dayjs'
import { ActualizarPedidoDto } from '../dto/actualizar-pedido.dto'
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity'

@Injectable()
export class PedidoRepository {
  constructor(private dataSource: DataSource) {}

  async buscarPorId(id: string) {
    return await this.dataSource
      .getRepository(Pedido)
      .createQueryBuilder('pedido')
      .where({ id: id })
      .getOne()
  }

  async listar(filtroPedidos: PedidoFiltro) {
    const { limite, saltar, fecha } = filtroPedidos
    const query = this.dataSource
      .getRepository(Pedido)
      .createQueryBuilder('pedido')
      .select([
        'pedido.id', //
        'pedido.nroPedido',
        'pedido.fechaPedido',
        'pedido.estado',
      ])
      .take(limite)
      .skip(saltar)

    if (fecha) {
      query.where('pedido.fechaPedido = :fecha', { fecha })
    }

    return await query.getManyAndCount()
  }

  async crear(datosPedido: CrearPedidoDto, usuarioAuditoria: string) {
    const datosGuardar: any = new Pedido({
      nroPedido: datosPedido.nroPedido,
      fechaPedido: dayjs(datosPedido.fechaPedido, 'YYYY-MM-DD').toDate(),
      usuarioCreacion: usuarioAuditoria,
    })
    const result = await this.dataSource
      .getRepository(Pedido)
      .save(datosGuardar)

    return result
  }

  async actualizar(
    id: string,
    pedidoDto: ActualizarPedidoDto,
    usuarioAuditoria: string
  ) {
    const datosActualizar: QueryDeepPartialEntity<Pedido> = new Pedido({
      ...pedidoDto,
      usuarioModificacion: usuarioAuditoria,
    })
    return await this.dataSource
      .getRepository(Pedido)
      .update(id, datosActualizar)
  }
}
