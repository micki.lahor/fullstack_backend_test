import { ApiProperty } from '@nestjs/swagger'
import { IsOptional } from 'class-validator'
import { PaginacionQueryDto } from 'src/common/dto/paginacion-query.dto'

export class PedidoFiltro extends PaginacionQueryDto {
  @ApiProperty({
    example: 'Fecha con formato YYYY-MM-DD',
    required: false,
  })
  @IsOptional()
  fecha?: string
}
