import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from '../../../common/validation'

export class CrearPedidoDto {
  @ApiProperty({ example: '1' })
  @IsNotEmpty()
  nroPedido: number

  @ApiProperty({ example: 'Fecha con formato YYYY-MM-DD' })
  fechaPedido: Date
}
