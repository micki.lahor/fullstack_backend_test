import { ProductosService } from '../../../application/productos/service'
import { BaseService } from '../../../common/base/base-service'
import { Inject, Injectable, NotFoundException } from '@nestjs/common'
import { PedidoRepository } from '../repository/pedido.repository'
import { CrearPedidoDto, PedidoFiltro } from '../dto'
import { ActualizarPedidoDto } from '../dto/actualizar-pedido.dto'
import { Messages } from 'src/common/constants/response-messages'
import { PedidoEstado } from '../constant'

@Injectable()
export class PedidoService extends BaseService {
  constructor(
    private productosService: ProductosService,

    @Inject(PedidoRepository)
    private pedidoRepositorio: PedidoRepository
  ) {
    super()
  }

  async crear(datosPedido: CrearPedidoDto, usuarioAuditoria: string) {
    const result = await this.pedidoRepositorio.crear(
      datosPedido,
      usuarioAuditoria
    )
    return result
  }

  async listar(filtro: PedidoFiltro) {
    const result = await this.pedidoRepositorio.listar(filtro)
    return result
  }

  async actualizarDatos(
    id: string,
    pedidoDto: ActualizarPedidoDto,
    usuarioAuditoria: string
  ) {
    const pedido = await this.pedidoRepositorio.buscarPorId(id)
    if (!pedido) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    await this.pedidoRepositorio.actualizar(id, pedidoDto, usuarioAuditoria)
    return { id }
  }

  async activar(idPedido: string, usuarioAuditoria: string) {
    const pedido = await this.pedidoRepositorio.buscarPorId(idPedido)
    if (!pedido) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const pedidoDto = new ActualizarPedidoDto()
    pedidoDto.estado = PedidoEstado.ACTIVO
    await this.pedidoRepositorio.actualizar(
      idPedido,
      pedidoDto,
      usuarioAuditoria
    )
    return {
      id: idPedido,
      estado: pedidoDto.estado,
    }
  }

  async inactivar(idPedido: string, usuarioAuditoria: string) {
    const pedido = await this.pedidoRepositorio.buscarPorId(idPedido)
    if (!pedido) {
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const pedidoDto = new ActualizarPedidoDto()
    pedidoDto.estado = PedidoEstado.INACTIVO
    await this.pedidoRepositorio.actualizar(
      idPedido,
      pedidoDto,
      usuarioAuditoria
    )
    return {
      id: idPedido,
      estado: pedidoDto.estado,
    }
  }
}
