import { Module } from '@nestjs/common'
import { MensajeriaModule } from './mensajeria/mensajeria.module'
import { ApiExternaModule } from './api-externa/api-externa.module'

@Module({
  imports: [MensajeriaModule, ApiExternaModule],
  providers: [],
  exports: [MensajeriaModule, ApiExternaModule],
})
export class ExternalServicesModule {}
