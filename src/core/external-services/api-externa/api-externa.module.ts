import { Module } from '@nestjs/common';
import { AxiosAdapter } from './adapters/axios.adapter';
import { ApiExternaService } from './api-externa.service';
import { ConfigModule } from '@nestjs/config';
import { ApiExternaController } from './api-externa.controller';

@Module({
    providers: [AxiosAdapter,ApiExternaService],
    exports: [AxiosAdapter,ApiExternaService],
    imports: [ConfigModule],
    controllers:[ApiExternaController]
})
export class ApiExternaModule {}