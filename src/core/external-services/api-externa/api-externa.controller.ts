import { Controller, Get, Param } from '@nestjs/common';
import { ApiExternaService } from './api-externa.service';

@Controller('zeus')
export class ApiExternaController {
  constructor(private readonly apiExternaService: ApiExternaService) {}

  @Get('departamentos')
  findAllDepartamentos() {
    return this.apiExternaService.getDepartamentoZeusPro();
  }

  @Get('municipios/:id')
  findAllMunicipios(@Param('id') id: number) {
    return this.apiExternaService.getMunicipioZeusPro(id);
  }

  @Get('oficinas/:id')
  findAllOficinas(@Param('id') id: number) {
    return this.apiExternaService.getOficinaZeusProPorMunicipio(id);
  }
}