import { printLogo } from './print-logo'

const DEFAULT_LOGO = `Proyecto Diplomado Fullstack Backend`

export function printNestJSLogo(logo = DEFAULT_LOGO) {
  printLogo(logo)
}
