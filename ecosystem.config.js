module.exports = {
  apps: [
    {
      name: 'fullstack_backend',
      script: './dist/src/main.js',
      instances: 1,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
}
