# Proyecto Diplomado Fullstack - Aseguramiento de la calidad y pruebas

<p>
  <a href="./">
    <img src="https://img.shields.io/badge/version-v1.0.1-blue" alt="Versión">
  </a>
  <a href="./LICENSE">
      <img src="https://img.shields.io/static/v1?label=license&message=LPG%20-%20Bolivia&color=green" alt="Licencia: LPG - Bolivia" />
  </a>
</p>

# Manual de instalación

## 1. Requerimientos

| Nombre       | Versión | Descripción                                            |
| ------------ | ------- | ------------------------------------------------------ |
| `PostgreSQL` | ^16     | Gestor de base de datos.                               |
| `NodeJS`     | ^20     | Entorno de programación de JavaScript.                 |
| `NPM`        | ^10     | Gestor de paquetes de NodeJS.                          |

## 2. Instalación

### Clonación del proyecto e instalación de dependencias

```bash
# Clonación del proyecto
git clone https://gitlab/micki.lahor/fullstack_backend.git

# Ingresamos dentro de la carpeta del proyecto
cd fullstack_backend

# Instalamos dependencias
npm install
```

### Archivos de configuración.

```bash
# Variables de entorno globales
cp .env.sample .env
```

### Creación y configuración de la Base de Datos

Restaurar en backup de la base de datos que se encuentra en la carpeta database/backup

### Despliegue de la aplicación

```bash

# Ejecución en modo desarrollo (live-reload)
npm run start:dev
```

### Ejecución de pruebas unitarias y de integración

```bash
# Todas las pruebas
npm run test

# Modulo por modulo
npm run test:usuario
npm run test:autorizacion
npm run test:parametrica
npm run test:common